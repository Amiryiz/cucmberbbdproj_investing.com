Feature: Searching of various categories should be possible along with the product search

  Scenario: Search for the products under Books category
    Given I visit the website as a Guest user
    When I select the book option from the dropdown
    And I click on search icon
    Then I should see the book page loaded
    And I should see Books at amazon as heading
    But i should not see the other category products

  Scenario: Search for the products under Baby category
    Given I visit the website as a Guest user
    When I select the baby option from the dropdown
    And I click on search icon
    Then I should see the baby page loaded
    And I should see The baby Store as heading
    But i should not see the other category products