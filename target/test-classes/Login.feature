Feature: Login to investing com site

  Scenario Outline: User should only be able to login with valid credential
    Given I navigate to investing com website
    When I enter Username as "<username>" and password as "<password>" into the fields
    And I click on Login button
    Then User should login base on expected "<loginstatus>" status

    Examples:
    |username          |password|loginstatus|
    |amiryiz@yahoo.com |123456ab|success    |
    |eli444fd@gmail.com|test777 |failure    |
    |moshe707@gmail.com|testAAA |failure    |

