package CucumberBDDPack;

import io.cucumber.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Login {

        WebDriver driver;

        @Given("I navigate to investing com website")
        public void iNavigateToInvestingComWebsite(){

                System.setProperty("webdriver.chrome.driver","drivers//chromedriver.exe");

                driver = new ChromeDriver();

                driver.manage().window().maximize();

                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

                driver.get("http://www.investing.com");

        }

        @When("^I enter Username as \"([^\"]*)\" and password as \"([^\"]*)\" into the fields$")
        public void iEnterUsernameAndPassword(String username,String password) {

                driver.findElement(By.xpath("//a[@class='login bold']")).click();
                driver.findElement(By.id("loginFormUser_email")).sendKeys(username);
                driver.findElement(By.id("loginForm_password")).sendKeys(password);

        }

        @And("I click on Login button")
        public void iClickOnLoginButton() {

                driver.findElement(By.xpath("//a[@class='newButton orange'][contains(text(),'Sign In')]")).click();
        }

        @Then("^User should login base on expected \"([^\"]*)\" status$")
        public void userShouldLoginBaseStatus(String expectedLoginAtatus) {

                String actualLoginStatus = null;
                String alertText;

                try{
                        alertText = driver.findElement(By.id("serverErrors")).getText();
                        if (alertText.equalsIgnoreCase("Wrong email or password. Try again."))
                                actualLoginStatus = "failure";

                }

                catch (Exception e) {

                        actualLoginStatus = "success";

                }

                if (actualLoginStatus.equalsIgnoreCase(expectedLoginAtatus)) {
                        //Test case will pass
                }
                else
                        Assert.fail("Login test has failed");

                driver.quit();

        }


}
